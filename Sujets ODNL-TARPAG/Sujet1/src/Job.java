
public class Job {

	public int[] tempsExecution;
	public int currentMachine;
	public boolean ended;
	public int chosenPlace;
	public int finished = 0;
	
	public Job(int nbMachines){
		chosenPlace = 0;
		
		tempsExecution = new int[nbMachines];
		for (int i = 0; i < nbMachines; i++)
		{
			tempsExecution[i] = nbMachines;
		}
		currentMachine = -1;
		ended = false;
		finished = 1;
	}

	public Job(Job job) {
		chosenPlace = job.chosenPlace;
		
		tempsExecution = new int[job.tempsExecution.length];
		for (int i = 0; i < job.tempsExecution.length; i++)
		{
			tempsExecution[i] = job.tempsExecution[i];
		}
		currentMachine = -1;
		ended = false;
		finished = 1;
	}

	public void print() {
		for (int i = 0; i < tempsExecution.length; i++)
		{
			System.out.print(tempsExecution[i] + " ");
		}
		System.out.println();
	}

	public void update(Job[] orderedJobs, int[] machinesBusy) {
		if (currentMachine == tempsExecution.length)
			return;
		if (chosenPlace == 0)
		{
			if (finished == 0 && machinesBusy[currentMachine] <= 1)
			{
				if (currentMachine != -1)
					machinesBusy[currentMachine] = -1;
				finished = 1;
			}
			if (finished == 1 && currentMachine == tempsExecution.length - 1)
			{
				machinesBusy[currentMachine] = -1;
				currentMachine = tempsExecution.length;
				return;
			}
			if (finished == 1 && machinesBusy[currentMachine + 1] == -1)
			{
				currentMachine++;
				finished = 0;
				machinesBusy[currentMachine] = tempsExecution[currentMachine];
			}
			else if (currentMachine != -1 && finished == 0)
				machinesBusy[currentMachine]--;
		}
		else
		{
			if (finished == 0 && machinesBusy[currentMachine] <= 1)
			{
				if (currentMachine != -1)
					machinesBusy[currentMachine] = -1;
				finished = 1;
			}
			if (finished == 1 && currentMachine == tempsExecution.length - 1)
			{
				currentMachine = tempsExecution.length;
				return;
			}
			if (finished == 1 && machinesBusy[currentMachine + 1] == -1)
			{
				if (orderedJobs[chosenPlace - 1].currentMachine >= currentMachine)
				{
					currentMachine++;
					finished = 0;
					machinesBusy[currentMachine] = tempsExecution[currentMachine];
				}
			}
			else if (currentMachine != -1 && finished == 0)
				machinesBusy[currentMachine]--;
		}
	}
}
