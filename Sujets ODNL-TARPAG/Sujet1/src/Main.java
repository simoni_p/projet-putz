import java.io.*;
import java.util.Random;

public class Main {
	
	public static int nbMachines;
	public static int nbJobs;
	public static int upperBound;
	public static Solution solution;
	public static Job[] jobs;
	
	public static void main(String[] args) {
		
		String fichierEntrée = "PB20x5_1.txt";
		
		InputStream ips;
		try
		{
			ips = new FileInputStream(fichierEntrée);
			
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			
			int nbLigne = 0;
			
			while ((ligne = br.readLine()) != null){
				
				if (nbLigne == 1)
				{
					String[] resul = ligne.split(" ");
					int iter = 0;
					for (int i = 0; i < resul.length; i++)
					{
						if (resul[i].compareTo("") != 0)
						{
							if (iter == 0)
							{
								nbJobs = Integer.valueOf(resul[i]).intValue();
							}
							if (iter == 1)
							{
								nbMachines = Integer.valueOf(resul[i]).intValue();
								
								jobs = new Job[nbJobs];
								for (int y = 0; y < nbJobs; y++)
								{
									jobs[y] = new Job(nbMachines);
								}
							}
							if (iter == 2)
							{
								Solution.generator = new Random();
								Solution.generator.setSeed(Integer.valueOf(resul[i]).longValue());
								System.out.println("seed is " + Integer.valueOf(resul[i]));
							}
							if (iter == 3)
							{
								upperBound = Integer.valueOf(resul[i]).intValue();
								System.out.println("solution is " + upperBound);
							}
							iter++;
						}
					}
				}
				nbLigne++;
				
				if (nbLigne - 4 >= 0 && nbLigne - 4 < nbMachines)
				{
					String[] resul = ligne.split(" ");
					int iter = 0;
					for (int i = 0; i < resul.length; i++)
					{
						if (resul[i].compareTo("") != 0)
						{
							System.out.print(resul[i] + " ");
							jobs[iter].tempsExecution[nbLigne - 4] = Integer.valueOf(resul[i]).intValue();
							iter++;
						}
					}
					System.out.println("");
				}
			}
			
			br.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("nbJobs = " + nbJobs);
		System.out.println("nbMachines = " + nbMachines);
		
		long startTime = System.currentTimeMillis();
		
		solution = new Solution(jobs);
		
		int timeMin = solution.getTiming();
		System.out.println("new best = " + timeMin);
		writeSolution();
		
		double temperature = 1000.0f;
		int acceptations = 0;
		int tests = 0;
		int noChange = 0;

		while (timeMin > 0)
		{
			Solution newSolution = new Solution(solution);
			newSolution.swapRandomPositions();
			int time = newSolution.getTiming();
			if (time == upperBound)
			{
				timeMin = time;
				solution = newSolution;
				writeSolution();
				break;
			}
			if (time < timeMin)
			{
				//System.out.println("new best = " + time);
				timeMin = time;
				solution = newSolution;
				writeSolution();
				acceptations++;
			}
			else
			{
				if (time >= timeMin && Math.random() < Math.exp(((double) (- (time - timeMin))) / temperature))
				{
					acceptations++;
					solution = newSolution;
					timeMin = time;
				}
			}
			tests++;
			if (acceptations > 12*nbJobs || tests > 100*nbJobs)
			{
				if (acceptations == 0)
					noChange++;
				else
					noChange = 0;
				temperature = temperature * 0.93;
				tests = 0;
				acceptations = 0;
				System.out.println("cycle    T = " + temperature);
				if (noChange >= 3)
					break;
			}
			long endTime = System.currentTimeMillis();
			long duration = (endTime - startTime);
			if ((double)duration / 1000 > 900)
				break;
		}
		
		long endTime = System.currentTimeMillis();
		long duration = (endTime - startTime);
		
		System.out.println("Duration = " + ((double)duration / 1000) + ", best = " + timeMin);
	}
	
	public static void writeSolution()
	{
		String fichierSortie ="fichierSortie.txt";
		
		try
		{
			FileWriter fw = new FileWriter (fichierSortie);
			BufferedWriter bw = new BufferedWriter (fw);
			PrintWriter fichierWriter = new PrintWriter (bw); 
			
			solution.writeInFile(fichierWriter);

			fichierWriter.close();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
