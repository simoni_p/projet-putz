import java.io.PrintWriter;
import java.util.Random;

public class Solution {

	public int[] orderOfJobs;
	public Job[] jobs;
	public static Random generator;
	public Job[] orderedJobs;
	
	public static int[] machinesBusy;
	
	public Solution(Job[] jobs) {
		
		orderOfJobs = new int[jobs.length];
		for (int i = 0; i < orderOfJobs.length; i++)
		{
			orderOfJobs[i] = i;
		}
		this.jobs = jobs;

		for (int i = 0; i < jobs.length; i++)
		{
			jobs[i].chosenPlace = i;
		}
	}

	public Solution(Solution solution) {
		orderOfJobs = new int[solution.orderOfJobs.length];
		jobs = new Job[solution.jobs.length];
		
		for (int i = 0; i < solution.orderOfJobs.length; i++)
		{
			orderOfJobs[i] = solution.orderOfJobs[i];
		}
		
		for (int i = 0; i < jobs.length; i++)
		{
			jobs[i] = new Job(solution.jobs[i]);
		}
		orderedJobs = new Job[jobs.length];
	}

	public void writeInFile(PrintWriter fichierWriter) {

		for (int r = 0; r < jobs.length; r++)
		{
			for (int i = 0; i < jobs.length; i++)
			{
				if (jobs[i].chosenPlace == r)
				{
					if (r == jobs.length - 1)
						fichierWriter.print(i);
					else
						fichierWriter.print(i + " ");
					break;
				}
			}
		}
	}
	
	public int getTiming()
	{
		int clock = 0;
		boolean finished = false;
		machinesBusy = new int[Main.nbMachines];
		
		for (int i = 0; i < Main.nbMachines; i++)
		{
			machinesBusy[i] = -1;
		}
		
		orderedJobs = new Job[jobs.length];
		
		for (int y = 0; y < jobs.length; y++)
		{
			for (int i = 0; i < jobs.length; i++)
			{
				if (jobs[i].chosenPlace == y)
				{
					jobs[i].currentMachine = -1;
					jobs[i].finished = 1;
					orderedJobs[y] = jobs[i];
					break;
				}
			}
		}
		
		/*for (int i = 0; i < orderedJobs.length; i++)
		{
			for (int y = 0; y < orderedJobs[i].tempsExecution.length; y++)
			{
				System.out.print(orderedJobs[i].tempsExecution[y] + " ");
			}
			System.out.println();
		}*/
		
		while (finished == false)
		{
			finished = true;
			
			for (int i = 0; i < orderedJobs.length; i++)
			{
				orderedJobs[i].update(orderedJobs, machinesBusy);
			}
			
			
			for (int t = 0; t < orderedJobs.length; t++)
			{
				if (orderedJobs[t].currentMachine != machinesBusy.length)
				{
					finished = false;
				}
			}
			
			if (finished)
				break;

			/*for (int t = 0; t < machinesBusy.length; t++)
			{
				System.out.print(machinesBusy[t] + " ");
			}
			System.out.println();*/
			clock++;
		}
		return clock;
	}

	public void swapRandomPositions() {
		
		int iter = 0;
		{
			int randA = (int) (generator.nextInt(orderedJobs.length));
			int randB = (int) (generator.nextInt(orderedJobs.length));
			
			while (randA == randB)
				randB = (int) (generator.nextInt(orderedJobs.length));
			
			int stock = jobs[randA].chosenPlace;
			jobs[randA].chosenPlace = jobs[randB].chosenPlace;
			jobs[randB].chosenPlace = stock;
			iter++;
		}
	}
}
