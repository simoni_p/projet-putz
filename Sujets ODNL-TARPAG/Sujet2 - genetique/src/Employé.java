import java.util.ArrayList;

public class Employé {

	public static int ID = 0;
	private int id;
	private ArrayList<Trajet> trajets;
	private int maxHours;
	private int maxDay;
	private int[] travailJour;
	private String location;
	
	public Employé() {
		trajets = new ArrayList<Trajet>();
		setMaxHours(5);
		setMaxDay(7);
		travailJour = new int[5];
		for (int i = 0; i < 5; i++)
			travailJour[i] = 0;
		id = ++ID;
		location = "";
	}
	
	public void addTrajet(Trajet t) {
		/*if ((t.getHeure_arrivée() - t.getHeure_départ()) > (maxHours - travailJour[t.getJour() - 1]) ||
				location != "" && t.getPoint_départ() != location)
			return false;
		else {
			travailJour[t.getJour() - 1] += (t.getHeure_arrivée() - t.getHeure_départ());*/
			trajets.add(t);
			/*location = t.getPoint_arrivée();
			return true;
		}*/
	}

	public String Output() {
		String tmp = "";
		
		for (int jour = 0; jour < 5; jour++)
		{
			boolean write = false;
			for (int j = 0; j < trajets.size(); j++) {
				if (trajets.get(j).getJour() == jour)
				{
					write = true;
					break;
				}
			}
			if (write)
			{
				tmp +=  id + " " + jour;
				for (int j = 0; j < trajets.size(); j++) {
					if (trajets.get(j).getJour() == jour)
						tmp += " " + trajets.get(j).getId();
				}
				tmp +=  System.getProperty("line.separator");
			}
		}

		return tmp;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Trajet> getTrajets() {
		return trajets;
	}

	public void setTrajets(ArrayList<Trajet> trajets) {
		this.trajets = trajets;
	}

	public int getMaxHours() {
		return maxHours;
	}

	public void setMaxHours(int maxHours) {
		this.maxHours = maxHours;
	}

	public int getMaxDay() {
		return maxDay;
	}

	public void setMaxDay(int maxDay) {
		this.maxDay = maxDay;
	}

	public void addTrajets(MetaTrajet metaTrajet) {
		for (int i = 0; i < metaTrajet.trajets.size(); i++)
			trajets.add(metaTrajet.trajets.get(i));
	}
}
