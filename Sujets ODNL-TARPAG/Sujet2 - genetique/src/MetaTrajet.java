import java.util.ArrayList;


public class MetaTrajet {
	public ArrayList<Trajet> trajets;
	public String pointDépart;
	public String PointArrivée;
	public Génome génes;
	public boolean taken;
	
	public MetaTrajet(){
		trajets = new ArrayList<Trajet>();
		génes = new Génome();
	}
	
	public MetaTrajet(MetaTrajet metaTrajet) {
		trajets = new ArrayList<Trajet>();
		for (int i = 0; i < metaTrajet.trajets.size(); i++)
			trajets.add(metaTrajet.trajets.get(i));
		pointDépart = metaTrajet.pointDépart;
		PointArrivée = metaTrajet.PointArrivée;
		génes = new Génome();
	}

	public MetaTrajet(Trajet trajet) {
		trajets = new ArrayList<Trajet>();
		génes = new Génome();
		trajets.add(trajet);
		pointDépart = trajet.getPoint_départ();
		PointArrivée = trajet.getPoint_arrivée();
	}

	public MetaTrajet(MetaTrajet metaTrajet, MetaTrajet metaTrajet2) {
		trajets = new ArrayList<Trajet>();
		génes = new Génome(metaTrajet.génes);
		
		for (int i = 0; i < metaTrajet.trajets.size(); i++)
			trajets.add(metaTrajet.trajets.get(i));
		
		for (int i = 0; i < metaTrajet2.trajets.size(); i++)
			trajets.add(metaTrajet2.trajets.get(i));
	}

	/*public static void combineTrajets(ArrayList<Trajet> trajets2, ArrayList<MetaTrajet> metaTrajets){
		
		for (int y = 0; y < trajets2.size(); y++)
		{
			for (int i = 0; i < trajets2.size(); i++)
			{
				if (i == y) continue;
				
				if (!trajets2.get(i).taken && !trajets2.get(y).taken && combinable(trajets2.get(y), trajets2.get(i)))
				{
					MetaTrajet newMeta = new MetaTrajet();
					
					newMeta.trajets.add(trajets2.get(y));
					newMeta.trajets.add(trajets2.get(i));
					newMeta.pointDépart = trajets2.get(y).getPoint_départ();
					newMeta.PointArrivée = trajets2.get(i).getPoint_arrivée();
					
					metaTrajets.add(newMeta);
					trajets2.get(y).taken = true;
					trajets2.get(i).taken = true;
				}
			}
		}
	}*/
	/*
	public static void combineRandomTrajets(ArrayList<Trajet> trajets2, ArrayList<MetaTrajet> metaTrajets){
		
		int i = (int) (Math.random() * trajets2.size());
		int y = (int) (Math.random() * trajets2.size());
		
		while (i == y)
			y = (int) (Math.random() * trajets2.size());
		
		if (!trajets2.get(i).taken && !trajets2.get(y).taken && combinable(trajets2.get(y), trajets2.get(i)))
		{
			MetaTrajet newMeta = new MetaTrajet();
			
			newMeta.trajets.add(trajets2.get(y));
			newMeta.trajets.add(trajets2.get(i));
			newMeta.pointDépart = trajets2.get(y).getPoint_départ();
			newMeta.PointArrivée = trajets2.get(i).getPoint_arrivée();
			
			metaTrajets.add(newMeta);
			trajets2.get(y).taken = true;
			trajets2.get(i).taken = true;
		}
	}*/
	
	public static void assignedOthersTrajets(ArrayList<Trajet> trajets2, ArrayList<MetaTrajet> metaTrajets){
		for (int y = 0; y < trajets2.size(); y++)
		{
			if (!trajets2.get(y).taken)
			{
				MetaTrajet newMeta = new MetaTrajet();
				newMeta.trajets.add(trajets2.get(y));
				metaTrajets.add(newMeta);
				trajets2.get(y).taken = true;
			}
		}
	}
	
	public static boolean combinable(Trajet trajet1, Trajet trajet2)
	{
		if (trajet1.getJour() == trajet2.getJour()
				&& trajet1.getPoint_arrivée().compareTo(trajet2.getPoint_départ()) == 0
				&& trajet1.getHeure_arrivée() <= trajet2.getHeure_départ()
				&& trajet2.getHeure_arrivée() - trajet1.getHeure_départ() < 7)
			return true;
		if (trajet1.getJour() == trajet2.getJour() -1
				&& trajet1.getPoint_arrivée().compareTo(trajet2.getPoint_départ()) == 0)
			return true;
		return false;
	}

	public static ArrayList<MetaTrajet> initializeFrom(ArrayList<MetaTrajet> listReference, ArrayList<Trajet> trajets2) {
		ArrayList<MetaTrajet> newList = new ArrayList<MetaTrajet>();
		
		for (int i = 0; i < listReference.size(); i++)
		{
			newList.add(new MetaTrajet(listReference.get(i)));
		}
		
		return newList;
	}
	
	public void debug()
	{
		System.out.println("nb trajets = " + trajets.size());
		génes.debug();
	}

	public static ArrayList<MetaTrajet> triggerGénome(ArrayList<MetaTrajet> individu) {
		ArrayList<MetaTrajet> newList = new ArrayList<MetaTrajet>();
		
		MetaTrajet newMetaTrajet;
		for (int i = 0; i < individu.size(); i++)
		{
			individu.get(i).taken = false;
		}
		
		for (int i = 0; i < individu.size(); i++)
		{
			for (int o = 0; o < individu.size(); o++)
			{
				if (i == o) continue;
				
				if (individu.get(i).génes.chromosomes[o] == 1)
				{
					if (!individu.get(i).taken && !individu.get(o).taken && combinable(individu.get(i), individu.get(o)))
					{
						newMetaTrajet = new MetaTrajet(individu.get(i), individu.get(o));
						newMetaTrajet.pointDépart = individu.get(i).pointDépart;
						newMetaTrajet.PointArrivée = individu.get(i).PointArrivée;
						newList.add(newMetaTrajet);
						individu.get(i).taken = true;
						individu.get(o).taken = true;
						break;
					}
				}
			}
		}
		
		for (int i = 0; i < individu.size(); i++)
		{
			if (!individu.get(i).taken)
			{
				MetaTrajet newMeta = new MetaTrajet(individu.get(i));
				newList.add(newMeta);
				individu.get(i).taken = true;
			}
		}
		
		return newList;
	}

	private static boolean combinable(MetaTrajet metaTrajet1, MetaTrajet metaTrajet2) {

		if (metaTrajet1.trajets.size() >= 1 && metaTrajet2.trajets.size() >= 1)
		{
			if (metaTrajet1.trajets.get(0).getJour() == metaTrajet2.trajets.get(0).getJour()
					&& metaTrajet1.trajets.get(0).getPoint_arrivée().compareTo(metaTrajet2.trajets.get(0).getPoint_départ()) == 0
					&& metaTrajet1.trajets.get(0).getHeure_arrivée() <= metaTrajet2.trajets.get(0).getHeure_départ()
					&& metaTrajet2.trajets.get(0).getHeure_arrivée() - metaTrajet1.trajets.get(0).getHeure_départ() < 7)
				return true;
			return combinable(metaTrajet1.trajets.get(metaTrajet1.trajets.size() - 1), metaTrajet2.trajets.get(0));
		}

		return false;
	}

	public void createNewGénes(int size) {
		génes = new Génome(size);
	}
}
