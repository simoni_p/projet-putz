import java.io.*;
import java.util.ArrayList;

public class Main {
	public static ArrayList<Trajet> trajets = new ArrayList<Trajet>();
	public static ArrayList<MetaTrajet> metaTrajets;
	public static ArrayList<Employé> employés;
	public static int nbrEmployés = 0;
	
	public static ArrayList<ArrayList<MetaTrajet>> listPopulation;
	public static ArrayList<MetaTrajet> bestPopulation;
	
	public static void main(String[] args_p) {
		Readfile("JDD1.txt");
		
		System.out.println("Before...");
		bestPopulation = new ArrayList<MetaTrajet>();
		
		for (int i = 0; i < trajets.size(); i++)
		{
			bestPopulation.add(new MetaTrajet(trajets.get(i)));
		}
		
		System.out.println("After");
		
		System.out.println("size after chromosomes = " + bestPopulation.size());
		
		
		radomizeGenome(bestPopulation);
		bestPopulation = MetaTrajet.triggerGénome(bestPopulation);
		
		System.out.println("size after chromosomes = " + bestPopulation.size());
		
		int bestValue = bestPopulation.size();
		System.out.println("New best = " + bestValue);
		
		int iter = 0;
		while (iter < 10)
		{
			iter++;
			listPopulation = new ArrayList<ArrayList<MetaTrajet>>();
			
			for (int i = 0; i < 100; i++)
			{
				ArrayList<MetaTrajet> individu = MetaTrajet.initializeFrom(bestPopulation, trajets);
				radomizeGenome(individu);
				individu = MetaTrajet.triggerGénome(individu);
				listPopulation.add(individu);
			}
			
			for (int i = 0; i < listPopulation.size(); i++)
			{
				if (listPopulation.get(i).size() < bestValue)
				{
					bestValue = listPopulation.get(i).size();
					bestPopulation = listPopulation.get(i);
					System.out.println("New best = " + bestValue + " iteration " + iter);
				}
			}
		}

		employés = new ArrayList<Employé>();
		
		AffectEmployesMetaTrajets();
		
		Writefile("Output.txt");
	}
	
	private static void radomizeGenome(ArrayList<MetaTrajet> individu) {
		
		for (int i = 0; i < individu.size(); i++)
		{
			individu.get(i).createNewGénes(individu.size());
			individu.get(i).génes.randomize();
		}
	}

	public static void Readfile(String file) {
		try {
			InputStream ips = new FileInputStream(file); 
			InputStreamReader ipsr = new InputStreamReader(ips);
			BufferedReader br = new BufferedReader(ipsr);
			String ligne;
			while ((ligne = br.readLine()) != null) {
				for (int j = 0; j < 5; j++)
				{
					Trajet t = new Trajet(ligne.split(" "), j);
					trajets.add(t);
				}
			}
			br.close(); 
		}
		catch (Exception e){
			System.out.println(e.toString());
		}
	}
	
	public static void AffectEmployesMetaTrajets() {
		for (int i = 0; i < bestPopulation.size(); i++) {
			Employé e = new Employé();
			e.addTrajets(bestPopulation.get(i));
			employés.add(e);
		}
		nbrEmployés = employés.size();
	}
	
	public static void Writefile(String file) {
		
		String fichierSortie = file;
		
		try
		{
			FileWriter fw = new FileWriter (fichierSortie);
			BufferedWriter bw = new BufferedWriter (fw);
			PrintWriter fichierWriter = new PrintWriter (bw); 
			String res2 = "";
			for (int i = 0; i < employés.size(); i++) {
				String res = employés.get(i).Output();
				res2 += res;
			}
			fichierWriter.print(res2);
			fichierWriter.close();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
