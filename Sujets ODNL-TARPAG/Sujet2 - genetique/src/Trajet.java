public class Trajet {
	
	private int id;
	private float heure_départ;
	private float heure_arrivée;
	private String point_départ;
	private String point_arrivée;
	private int jour;
	public boolean taken = false;
	
	public Trajet(String[] data, int jour) {
		id = Integer.parseInt(data[0]);
		point_départ = data[1];
		point_arrivée = data[2];
		heure_départ = ConvertHour(data[3]);
		heure_arrivée = ConvertHour(data[4]);
		this.jour = jour;
		taken = false;
	}

	private float ConvertHour(String hour) {
		String[] tmp = hour.split(":");
		float res = Integer.parseInt(tmp[0]);
		res += Float.parseFloat(tmp[1]) / 60;
		return res;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public float getHeure_départ() {
		return heure_départ;
	}
	
	public void setHeure_départ(int heure_départ) {
		this.heure_départ = heure_départ;
	}
	
	public float getHeure_arrivée() {
		return heure_arrivée;
	}
	
	public void setHeure_arrivée(int heure_arrivée) {
		this.heure_arrivée = heure_arrivée;
	}
	
	public String getPoint_départ() {
		return point_départ;
	}
	
	public void setPoint_départ(String point_départ) {
		this.point_départ = point_départ;
	}
	
	public String getPoint_arrivée() {
		return point_arrivée;
	}
	
	public void setPoint_arrivée(String point_arrivée) {
		this.point_arrivée = point_arrivée;
	}

	public int getJour() {
		return jour;
	}

	public void setJour(int jour) {
		this.jour = jour;
	}

	public String getString() {
		return jour + " " + id;
	}
}
