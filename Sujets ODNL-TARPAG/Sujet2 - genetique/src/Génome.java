import java.util.ArrayList;


public class Génome {
	
	public int nbChromosomes;
	public int[] chromosomes;
	
	public Génome(Génome génes) {
		chromosomes = new int[génes.nbChromosomes];
		for (int i = 0; i < génes.nbChromosomes; i++)
		{
			chromosomes[i] = génes.chromosomes[i];
		}
	}

	public Génome() {
	}
	
	public Génome(int nb) {
		nbChromosomes = nb;
		chromosomes = new int[nbChromosomes];
		for (int i = 0; i < nbChromosomes; i++)
		{
			chromosomes[i] = 0;
		}
	}
	
	public void debug()
	{
		for (int i = 0; i < nbChromosomes; i++)
		{
			System.out.print(chromosomes[i] + " ");
		}
		System.out.println();
	}

	public void randomize() {
		for (int i = 0; i < nbChromosomes; i++)
		{
			if (Math.random() < 0.8)
			{
				if (chromosomes[i] == 0)
					chromosomes[i] = 1;
				else if (chromosomes[i] == 1)
					chromosomes[i] = 0;
			}
		}
	}
}
